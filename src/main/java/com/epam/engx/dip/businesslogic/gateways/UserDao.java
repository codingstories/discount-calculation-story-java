package com.epam.engx.dip.businesslogic.gateways;

import com.epam.engx.dip.businesslogic.User;
import com.epam.engx.dip.persistance.models.UserModel;

public interface UserDao {

	User getUserById(int id);
	
}
