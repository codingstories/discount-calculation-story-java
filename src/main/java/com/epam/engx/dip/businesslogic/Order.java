package com.epam.engx.dip.businesslogic;

import java.math.BigDecimal;
import java.math.RoundingMode;

public abstract class Order {
	
	public static final int MONEY_VALUE_SCALE = 2;

	public BigDecimal getGrossOrderPrice() {
		return BigDecimal.valueOf(getGrossOrderPriceRaw())
				.setScale(MONEY_VALUE_SCALE, RoundingMode.HALF_UP);
	}

	private double getGrossOrderPriceRaw() {
		return getTotalTax() + getOrderPrice();
	}; 
	
	public abstract double getOrderPrice();
	
	public abstract double getTotalTax();
	
	public abstract void setOrderPrice(double orderPrice);
	
	public abstract void setTotalTax(double totalTax);
	
}
