package com.epam.engx.dip.persistance.models;

import java.sql.Date;
import java.util.List;

public class OrderModel {

	private int id;
	private double orderPrice;
	private List<ProductModel> products;
	private Date purchaseDate;
	private int buyerId;
	private double discount;
	private double totalTax;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public double getOrderPrice() {
		return orderPrice;
	}
	public void setOrderPrice(double orderPrice) {
		this.orderPrice = orderPrice;
	}
	public List<ProductModel> getProducts() {
		return products;
	}
	public void setProducts(List<ProductModel> products) {
		this.products = products;
	}
	public Date getPurchaseDate() {
		return purchaseDate;
	}
	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}
	public int getBuyerId() {
		return buyerId;
	}
	public void setBuyerId(int buyerId) {
		this.buyerId = buyerId;
	}
	public double getDiscount() {
		return discount;
	}
	public void setDiscount(double discount) {
		this.discount = discount;
	}
	public double getTotalTax() {
		return this.totalTax;
	}
	public void setTotalTax(double tax) {
		this.totalTax = tax;
	}
	
}
