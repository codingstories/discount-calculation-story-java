package com.epam.engx.dip.persistance.dao;

import java.util.Collections;
import java.util.stream.Collectors;

import org.hibernate.SessionFactory;

import com.epam.engx.dip.businesslogic.User;
import com.epam.engx.dip.businesslogic.gateways.UserDao;
import com.epam.engx.dip.persistance.PersistenceOrder;
import com.epam.engx.dip.persistance.PersistenceUser;
import com.epam.engx.dip.persistance.models.OrderModel;
import com.epam.engx.dip.persistance.models.UserModel;

public class DefaultUserDao implements UserDao {

	private SessionFactory sessionFactory;

	@Override
	public User getUserById(int id) {
		UserModel userModel = getUserModelById(id);
		PersistenceUser user = new PersistenceUser();
		user.setId(userModel.getId());
		populateUserWithOrders(userModel, user);
		return user;
	}

	private UserModel getUserModelById(int id) {
		return sessionFactory.getCurrentSession().get(UserModel.class, id);
	}

	private void populateUserWithOrders(UserModel userModel, PersistenceUser user) {
		if (userModel.getOrderModels() == null) {
			user.setOrders(Collections.EMPTY_LIST);
		} else {
			user.setOrders(userModel.getOrderModels().stream()
					.map(orderModel -> convertOrderModelToOrder(orderModel))
					.collect(Collectors.toList()));
		}
	}
	
	private PersistenceOrder convertOrderModelToOrder(OrderModel orderModel) {
		PersistenceOrder order = new PersistenceOrder();
		order.setId(orderModel.getId());
		order.setOrderPrice(orderModel.getOrderPrice());
		order.setTotalTax(orderModel.getTotalTax());
		return order;
	}
}
