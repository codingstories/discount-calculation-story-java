package com.epam.engx.dip.persistance;

import java.util.List;

import com.epam.engx.dip.businesslogic.Order;
import com.epam.engx.dip.businesslogic.User;


public class PersistenceUser extends User {
	
	private int id;
	private List<Order> orders;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}

	@Override
	public List<Order> getOrders() {
		return this.orders;
	}

}
