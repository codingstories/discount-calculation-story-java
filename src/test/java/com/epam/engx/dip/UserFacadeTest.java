package com.epam.engx.dip;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.engx.dip.businesslogic.Order;
import com.epam.engx.dip.businesslogic.User;
import com.epam.engx.dip.businesslogic.gateways.UserDao;
import com.epam.engx.dip.persistance.PersistenceOrder;
import com.epam.engx.dip.persistance.PersistenceUser;
import com.epam.engx.dip.persistance.models.OrderModel;
import com.epam.engx.dip.persistance.models.UserModel;

@RunWith(MockitoJUnitRunner.class)
public class UserFacadeTest {

	private static final double ACCEPTABLE_DELTA_FOR_DOUBLE_COMPARISON = 0.001;

	private static final double ZERO_PERCENT_DISCOUNT = 0;
	private static final double FIVE_PERCENT_DISCOUNT = 0.05;
	private static final double TEN_PERCENT_DISCOUNT = 0.1;
	private static final double FIFTEEN_PERCENT_DISCOUNT = 0.15;

	private static final double TEST_ORDER_PRICE_LESS_THAN_LEVEL_1_THRESHOLD = 100;
	private static final double TEST_ORDER_PRICE_MORE_THAN_LEVEL_1_THRESHOLD = 1001;
	private static final double TEST_ORDER_PRICE_MORE_THAN_LEVEL_2_THRESHOLD = 3001;
	private static final double TEST_ORDER_PRICE_MORE_THAN_LEVEL_3_THRESHOLD = 5001;
	private static final double TEST_TAX = 100;
	
	private static final double TEST_ORDER_PRICE_LESS_THAN_LEVEL_1_THRESHOLD_WITHOUT_TAX = 900;
	
	private static final int TEST_USER_ID = 1;

	@InjectMocks
	private UserFacade testInstance;

	@Mock
	private UserDao mockUserDao;
	
	private Order order;
	
	private User user;

	@Before
	public void setUp() {
		order = new PersistenceOrder();
		user = new PersistenceUser();
	}
	
	@Test
	public void shouldCalculateZeroPercentDiscountForUserById() {
		order.setOrderPrice(TEST_ORDER_PRICE_LESS_THAN_LEVEL_1_THRESHOLD);
		order.setTotalTax(TEST_TAX);
		user.setOrders(new ArrayList(Arrays.asList(order)));
		
		when(mockUserDao.getUserById(TEST_USER_ID)).thenReturn(user);
		
		assertEquals(ZERO_PERCENT_DISCOUNT,
				testInstance.getDiscountForUserById(TEST_USER_ID).doubleValue(),
				ACCEPTABLE_DELTA_FOR_DOUBLE_COMPARISON);
	}

	@Test
	public void shouldCalculateFivePercentDiscountForUserById() {
		order.setOrderPrice(TEST_ORDER_PRICE_MORE_THAN_LEVEL_1_THRESHOLD);
		order.setTotalTax(TEST_TAX);
		user.setOrders(new ArrayList(Arrays.asList(order)));
		
		when(mockUserDao.getUserById(TEST_USER_ID)).thenReturn(user);
		
		assertEquals(FIVE_PERCENT_DISCOUNT,
				testInstance.getDiscountForUserById(TEST_USER_ID).doubleValue(),
				ACCEPTABLE_DELTA_FOR_DOUBLE_COMPARISON);
	}

	@Test
	public void shouldCalculateTenPercentDiscountForUserById() {
		order.setOrderPrice(TEST_ORDER_PRICE_MORE_THAN_LEVEL_2_THRESHOLD);
		order.setTotalTax(TEST_TAX);
		user.setOrders(new ArrayList(Arrays.asList(order)));
		
		when(mockUserDao.getUserById(TEST_USER_ID)).thenReturn(user);
		
		assertEquals(TEN_PERCENT_DISCOUNT,
				testInstance.getDiscountForUserById(TEST_USER_ID).doubleValue(),
				ACCEPTABLE_DELTA_FOR_DOUBLE_COMPARISON);
	}

	@Test
	public void shouldCalculateFifteenPercentDiscountForUserById() {
		order.setOrderPrice(TEST_ORDER_PRICE_MORE_THAN_LEVEL_3_THRESHOLD);
		order.setTotalTax(TEST_TAX);
		user.setOrders(new ArrayList(Arrays.asList(order)));
		
		when(mockUserDao.getUserById(TEST_USER_ID)).thenReturn(user);
		
		assertEquals(FIFTEEN_PERCENT_DISCOUNT,
				testInstance.getDiscountForUserById(TEST_USER_ID).doubleValue(),
				ACCEPTABLE_DELTA_FOR_DOUBLE_COMPARISON);
	}
	
	@Test
	public void shouldCalculateFivePercentDiscountWhenNetPriceIsLessThanThresholdButWithTaxTogetherIsMoreThanThreshold() {
		order.setOrderPrice(TEST_ORDER_PRICE_LESS_THAN_LEVEL_1_THRESHOLD_WITHOUT_TAX);
		order.setTotalTax(TEST_TAX);
		user.setOrders(new ArrayList(Arrays.asList(order)));
		
		when(mockUserDao.getUserById(TEST_USER_ID)).thenReturn(user);
		
		assertEquals(FIVE_PERCENT_DISCOUNT,
				testInstance.getDiscountForUserById(TEST_USER_ID).doubleValue(),
				ACCEPTABLE_DELTA_FOR_DOUBLE_COMPARISON);
	}
	
	@Test
	public void shouldCalculateTenPercentDiscountIfUserHadMultipleOrders() {
		order.setOrderPrice(TEST_ORDER_PRICE_MORE_THAN_LEVEL_1_THRESHOLD);
		order.setTotalTax(TEST_TAX);
		user.setOrders(new ArrayList(Arrays.asList(order, order, order)));
		
		when(mockUserDao.getUserById(TEST_USER_ID)).thenReturn(user);
		
		assertEquals(TEN_PERCENT_DISCOUNT,
				testInstance.getDiscountForUserById(TEST_USER_ID).doubleValue(),
				ACCEPTABLE_DELTA_FOR_DOUBLE_COMPARISON);
	}
	
	@Test
	public void shouldCalculateZeroPercentDiscountIfUserHasNotAnyOrdersInPast() {
		user.setOrders(Collections.EMPTY_LIST);
		
		when(mockUserDao.getUserById(TEST_USER_ID)).thenReturn(user);
		
		assertEquals(ZERO_PERCENT_DISCOUNT,
				testInstance.getDiscountForUserById(TEST_USER_ID).doubleValue(),
				ACCEPTABLE_DELTA_FOR_DOUBLE_COMPARISON);
	}

}
